# Project Poker in PHP

Project simulates the game of poker. The same was developed for the vacancy of the inovadora company.

### Prerequisites

The only prerequisite is php version 7.2.10

### Installing

Once you have downloaded the project you will need the execution permission in the pokerinit file

```
chmod +x pokerinit
```

## Commands

The system has two commands, follow them below.

### Random game with 2 random people.

Command random

```
./pokerinit random
```

Return

    Please wait...
    +----------+----------------+
    | Username | Hand           |
    +----------+----------------+
    | Minerva  | A♥ 2♦ J♠ 4♦ Q♠ |
    | Alvo     | 9♦ K♦ 5♣ K♥ K♥ |
    +----------+----------------+
    The results were as follows:
    Winner: Alvo
    Three of a kind, oh year!

### Starting the game by putting people

Command start [number of participants] [name of participants, separated by space]

```
./pokerinit start 2 Eu Tu
```

Return

    Although names not enough, i still execute the program, thanks me.
    Please wait...
    +----------+-----------------+
    | Username | Hand            |
    +----------+-----------------+
    | Eu       | 5♣ 3♠ K♠ 7♦ 8♥  |
    | Tu       | 9♠ J♣ 5♥ 7♠ 10♣ |
    +----------+-----------------+
    The results were as follows:
    Winner: Tu
    High card, i am so luck

## Authors

* **Renê Soares ** - *Initial work*

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE) file for details
